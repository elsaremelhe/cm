package com.example.cmapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.media.Image;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.cmapp.Classes.ClusterMarker;
import com.example.cmapp.Util.ClusterManagerRenderer;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.maps.android.clustering.ClusterManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import static com.example.cmapp.Util.Constants.ERROR_DIALOG_REQUEST;
import static com.example.cmapp.Util.Constants.MAPVIEW_BUNDLE_KEY;
import static com.example.cmapp.Util.Constants.PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION;
import static com.example.cmapp.Util.Constants.PERMISSIONS_REQUEST_ENABLE_GPS;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "MapActivity";
    private boolean mLocationPermissionGranted = false;

    private FusedLocationProviderClient fusedLocationClient;

    private MapView mv;

    private ClusterManager clusterManager;
    private ClusterManagerRenderer cmr;
    private ArrayList<ClusterMarker> clusterMarkers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        //create map
        Bundle mvBundle = null;
        if(savedInstanceState != null){
            mvBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }

        mv = (MapView) findViewById(R.id.mapView);
        mv.onCreate(mvBundle);

        mv.getMapAsync(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_log_out,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id == R.id.action_logout){
            Toast.makeText(MapActivity.this, R.string.logout, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(MapActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            MapActivity.this.finish();
        }

        return super.onOptionsItemSelected(item);
    }


    //checks if everything is ok
    private boolean checkMapServices(){
        if(isServicesOK()){
            if(isMapsEnabled()){
                return true;
            }
        }
        return false;
    }

    //prompt user to turn on gps (3)
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("This application requires GPS to work properly, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        Intent enableGpsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivityForResult(enableGpsIntent, PERMISSIONS_REQUEST_ENABLE_GPS);
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    //check if gps is enabled (2)
    public boolean isMapsEnabled(){
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps(); //call method to turn on gps
            return false;
        }
        return true;
    }

    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
            //getChatrooms(); display map
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    //check if google services available (1)
    public boolean isServicesOK(){
        Log.d(TAG, "isServicesOK: checking google services version");

        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(MapActivity.this);

        if(available == ConnectionResult.SUCCESS){
            //everything is fine and the user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(available)){
            //an error occured but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(MapActivity.this, available, ERROR_DIALOG_REQUEST);
            dialog.show();
        }else{
            Toast.makeText(this, "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    @Override //run after permission request on getLocationPermission(), set mLocation = true
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
    }

    @Override //checks gps activity result (4)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: called.");
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ENABLE_GPS: {
                if(!mLocationPermissionGranted){
                    getLocationPermission();
                }
            }
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        if(checkMapServices()){
            if(!mLocationPermissionGranted){
                getLocationPermission();
            }
        }
        mv.onResume();
    }


    @Override
    public void onStart() {
        super.onStart();
        mv.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        mv.onStop();
    }

    @Override
    public void onMapReady(final GoogleMap map) {

        final String userid = getIntent().getExtras().getString("id");
        final String username = getIntent().getExtras().getString("name");

        map.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
        map.setMyLocationEnabled(true);

        //get last known location
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(final Location location) {
                        // Got last known location. (can be null in reeeally rare situations)
                        if (location != null) {
                            map.moveCamera((CameraUpdateFactory.newLatLng(new LatLng(
                                    location.getLatitude(),
                                    location.getLongitude()
                            ))));
                            map.animateCamera(CameraUpdateFactory.zoomTo(20f));

                            findViewById(R.id.fabAddMarker).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(MapActivity.this, NewMarkerActivity.class);
                                    Bundle extras = new Bundle();
                                    extras.putString("lat", String.valueOf(location.getLatitude()));
                                    extras.putString("long", String.valueOf(location.getLongitude()));
                                    extras.putString("idUser", userid);
                                    extras.putString("nameUser", username);
                                    intent.putExtras(extras);
                                    startActivity(intent);
                                }
                            });

                            findViewById(R.id.fabListMarkers).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(MapActivity.this, ListMarkers.class);
                                    Bundle extras = new Bundle();
                                    extras.putString("idUser", userid);
                                    extras.putString("nameUser", username);
                                    Log.d("idMap", userid);
                                    intent.putExtras(extras);
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                });

        addMarkers(map, userid);



        map.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng point) {

                Log.d("useridmap", userid);

                Intent intent = new Intent(MapActivity.this, NewMarkerActivity.class);
                Bundle extras = new Bundle();
                extras.putString("lat", String.valueOf(point.latitude));
                extras.putString("long", String.valueOf(point.longitude));
                extras.putString("idUser", userid);
                extras.putString("nameUser", username);
                intent.putExtras(extras);
                startActivity(intent);

                //MarkerOptions marker = new MarkerOptions().position(new LatLng(point.latitude, point.longitude)).title("New Marker");
                //map.addMarker(marker);
                //System.out.println(point.latitude+"---"+ point.longitude);
            }
        });

    }

    private void addMarkers(GoogleMap map, final String id){
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Loading...");
        pd.show();

        if(map != null){
            if(clusterManager == null){
                clusterManager = new ClusterManager<ClusterMarker>(getApplicationContext(), map);

            }
            if(cmr == null){
                cmr = new ClusterManagerRenderer(this, map, clusterManager);
            }

            clusterManager.setRenderer(cmr);
        }

        final RequestQueue queue = Volley.newRequestQueue(this);

        final JsonArrayRequest getMarkersRequest = new JsonArrayRequest(Request.Method.GET, "http://192.168.1.3:3000/getmarkers", null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        pd.dismiss();

                        for(int i = 0; i<response.length(); i++){
                            try {
                                JSONObject c = response.getJSONObject(i);

                                try{
                                    String title = "";
                                    String snippet = c.getString("desc");
                                    LatLng ll = new LatLng(c.getDouble("lat"), c.getDouble("long"));
                                    int wh = (int)getApplicationContext().getResources().getDimension(R.dimen.marker);
                                    Bitmap decodedByte = Bitmap.createBitmap(wh, wh, Bitmap.Config.ARGB_8888);//default icon

                                    if(id.equals(c.getString("idUser"))){
                                        title = getString(R.string.markerTitleYou);
                                    }else{
                                        title = c.getString("nameUser");
                                    }

                                    try{
                                        byte[] decodedString = Base64.decode(c.getString("img"), Base64.DEFAULT);
                                        decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                                    }catch(NumberFormatException e){
                                        Log.d("avatar",e.getMessage());
                                    }

                                    ClusterMarker newClusterMarker = new ClusterMarker(
                                            ll,
                                            title,
                                            snippet,
                                            decodedByte,
                                            c.getString("_id")
                                    );

                                    clusterManager.addItem(newClusterMarker);
                                    clusterMarkers.add(newClusterMarker);


                                }catch(NullPointerException e){
                                    Log.e("markers", e.getMessage());
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        clusterManager.cluster();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MapActivity.this, R.string.error, Toast.LENGTH_SHORT).show();
                        Log.d("Error.Response", error.toString());
                    }
                }
        );

        queue.add(getMarkersRequest);
    }


    @Override
    public void onPause() {
        mv.onPause();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        mv.onDestroy();
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mv.onLowMemory();
    }



}
