package com.example.cmapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.cmapp.Retrofit.IMyService;
import com.example.cmapp.Retrofit.RetrofitClient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Retrofit;

import static com.example.cmapp.Util.Constants.CAMERA_PERMISSION_CODE;
import static com.example.cmapp.Util.Constants.CAMERA_REQUEST_CODE;

public class NewMarkerActivity extends AppCompatActivity {

    MaterialEditText markerDesc;
    ImageView markerImg;
    FloatingActionButton addMarkerBtn, cancelMarkerBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_marker);

        final String id = getIntent().getExtras().getString("idUser");
        final String name = getIntent().getExtras().getString("nameUser");
        final String lat = getIntent().getExtras().getString("lat");
        final String lon = getIntent().getExtras().getString("long");

        Log.d("useridM1", id);

        markerDesc = (MaterialEditText)findViewById(R.id.markerDesc);
        markerImg = (ImageView) findViewById(R.id.markerImage);
        addMarkerBtn = (FloatingActionButton) findViewById(R.id.addMarkerBtn);
        cancelMarkerBtn = (FloatingActionButton) findViewById(R.id.cancelMarkerBtn);

        cancelMarkerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewMarkerActivity.this, MapActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle bun = new Bundle();
                bun.putString("id", id);
                bun.putString("name", name);
                intent.putExtras(bun);
                startActivity(intent);
                NewMarkerActivity.this.finish();
            }
        });

        markerImg.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                askCamPermission();
            }
        });

        addMarkerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bitmap bm = ((BitmapDrawable)markerImg.getDrawable()).getBitmap();
                addMarker(markerDesc.getText().toString(), bm, id, lat, lon, name);
            }
        });

    }

    private void askCamPermission(){

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){

            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
        }else{
            openCam();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == CAMERA_PERMISSION_CODE){
            if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                openCam();
            }else{
                Toast.makeText(this, "Camera permission required", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void openCam(){
        Intent camIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(camIntent, CAMERA_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST_CODE) {
            Bitmap image = (Bitmap) data.getExtras().get("data");
            markerImg.setImageBitmap(image);

        }
    }

    private String imageToString(Bitmap bm){

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        byte[] imgBytes = outputStream.toByteArray();

        String encodedImg = Base64.encodeToString(imgBytes, Base64.DEFAULT);

        return encodedImg;
    }

    private void addMarker(final String sDesc, final Bitmap sImg, final String id, final String lat, final String lon, final String name){
        if(TextUtils.isEmpty(sDesc) || sImg.getWidth() == 0){
            Toast.makeText(this, R.string.markerEditorError, Toast.LENGTH_SHORT).show();
            return;
        }

        Log.d("useridM2", id);

        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest addMarkerRequest = new StringRequest(Request.Method.POST, "http://192.168.1.3:3000/addMarker",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(NewMarkerActivity.this, "yay c:", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(NewMarkerActivity.this, MapActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        Bundle bun = new Bundle();
                        bun.putString("id", id);
                        bun.putString("name", name);
                        intent.putExtras(bun);
                        startActivity(intent);
                        NewMarkerActivity.this.finish();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(NewMarkerActivity.this, R.string.markerImageError, Toast.LENGTH_SHORT).show();
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                String imgData = imageToString(sImg);

                params.put("idUser", id);
                params.put("name", name);
                params.put("desc", sDesc);
                params.put("img", imgData);
                params.put("lat", lat);
                params.put("long", lon);

                return params;
            }
        };


        queue.add(addMarkerRequest);
    }
}
