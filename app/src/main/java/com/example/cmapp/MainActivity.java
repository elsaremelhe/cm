package com.example.cmapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.cmapp.Adapters.NotesAdapter;
import com.example.cmapp.DB.DBHelper;

import java.util.List;

import com.example.cmapp.Classes.Note;
import com.example.cmapp.Util.Ajuda;

public class MainActivity extends AppCompatActivity {


    RecyclerView rvNotes;

    NotesAdapter adapter;
    List<Note> notesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvNotes = (RecyclerView) findViewById(R.id.rvNotes);
        findViewById(R.id.fabAddNote).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNote();
            }
        });

        setTitle(R.string.noteList);


        initViews();
        loadNotes();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_log_out,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id == R.id.action_logout){
            Toast.makeText(MainActivity.this, R.string.logout, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            MainActivity.this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViews() {

        rvNotes.setLayoutManager(new LinearLayoutManager(this));
    }

    private void loadNotes(){
        DBHelper db = new DBHelper(this);

        notesList = db.getAllNotes();
        if(notesList.size() != 0){
            adapter = new NotesAdapter(this,notesList);
            rvNotes.setAdapter(adapter);
        }


    }

    public void addNote(){
        Intent i = new Intent(MainActivity.this, NewNoteActivity.class);
        startActivity(i);
    }


    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        DBHelper db = new DBHelper(this);

        switch(item.getItemId()){
            case 0:
                Intent i = new Intent(MainActivity.this, EditNoteActivity.class);
                Ajuda.getInstance().notesAdapter = this.adapter;
                startActivity(i);
                return true;
            case 1:
                db.deleteNote(adapter.note.getId());
                adapter.removeItem();
                Toast.makeText(this, R.string.notedeleted, Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}