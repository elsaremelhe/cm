package com.example.cmapp.Util;

import com.example.cmapp.Adapters.NotesAdapter;

public class Ajuda {
    public NotesAdapter notesAdapter;
    private static Ajuda instance;

    private Ajuda() {

    }

    public static Ajuda getInstance() {
        if(instance == null) {
            instance = new Ajuda();
        }
        return instance;
    }
}
