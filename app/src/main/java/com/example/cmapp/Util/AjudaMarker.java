package com.example.cmapp.Util;

import com.example.cmapp.Adapters.MarkersAdapter;

public class AjudaMarker {
    public MarkersAdapter markersAdapter;
    private static AjudaMarker instance;

    private AjudaMarker() {

    }

    public static AjudaMarker getInstance() {
        if(instance == null) {
            instance = new AjudaMarker();
        }
        return instance;
    }
}
