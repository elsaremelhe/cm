package com.example.cmapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.cmapp.Adapters.MarkersAdapter;
import com.example.cmapp.Adapters.NotesAdapter;
import com.example.cmapp.Classes.ClusterMarker;
import com.example.cmapp.Classes.Note;
import com.example.cmapp.DB.DBHelper;
import com.example.cmapp.Util.AjudaMarker;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListMarkers extends AppCompatActivity {

    RecyclerView rvMarker;
    MarkersAdapter adapter;

    //MarkersAdapter adapter;
    List<ClusterMarker> markerList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_markers);
        rvMarker = (RecyclerView) findViewById(R.id.rvMarkers);
        rvMarker.setHasFixedSize(true);
        rvMarker.setLayoutManager(new LinearLayoutManager(this));

        setTitle(R.string.markerList);



        markerList = new ArrayList<>();


        initViews();
        loadMarkers();




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_go_back,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        final String userid = getIntent().getExtras().getString("idUser");
        final String username = getIntent().getExtras().getString("nameUser");

        int id = item.getItemId();
        if(id == R.id.action_back){
            Intent intent = new Intent(ListMarkers.this, MapActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            Bundle extras = new Bundle();
            extras.putString("id", userid);
            extras.putString("name", username);
            intent.putExtras(extras);
            startActivity(intent);
            ListMarkers.this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    private void initViews() {

        rvMarker.setLayoutManager(new LinearLayoutManager(this));
    }

    private void loadMarkers(){

        RequestQueue queue = Volley.newRequestQueue(this);

        final String idUser = getIntent().getExtras().getString("idUser");

        StringRequest getMarkerByUserRequest = new StringRequest(Request.Method.POST, "http://192.168.1.3:3000/getmarkersbyuser",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       // Toast.makeText(ListMarkers.this, "yay c:", Toast.LENGTH_SHORT).show();
                        JSONArray ja = null;
                        try {
                            ja = new JSONArray(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        for(int i = 0; i<ja.length(); i++){
                            try {
                                JSONObject c = ja.getJSONObject(i);

                                byte[] decodedString = Base64.decode(c.getString("img"), Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                                ClusterMarker marker = new ClusterMarker(
                                        new LatLng(c.getDouble("lat"), c.getDouble("long")),
                                        c.getString("nameUser"),
                                        c.getString("desc"),
                                        decodedByte,
                                        c.getString("_id")
                                );
                                markerList.add(marker);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        adapter = new MarkersAdapter(ListMarkers.this,markerList);
                        rvMarker.setAdapter(adapter);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(ListMarkers.this, R.string.error, Toast.LENGTH_SHORT).show();
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("idUser", idUser);

                return params;
            }
        };

        queue.add(getMarkerByUserRequest);


    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        switch(item.getItemId()){
            case 0:
                String idUser = getIntent().getExtras().getString("idUser");
                Intent i = new Intent(ListMarkers.this, EditMarkerActivity.class);
                AjudaMarker.getInstance().markersAdapter = this.adapter;
                Bundle bun = new Bundle();
                bun.putString("idUser", idUser);
                i.putExtras(bun);
                startActivity(i);
                return true;
            case 1:
                adapter.removeItem();
                Toast.makeText(this, R.string.markerdeleted, Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }


}
