package com.example.cmapp.Retrofit;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface IMyService {

    @POST("register")
    @FormUrlEncoded
    Observable<String> registerUser(@Field("name") String name,
                                    @Field("email") String email,
                                    @Field("pswd") String pswd);

    @POST("login")
    @FormUrlEncoded
    Observable<String> loginUser(@Field("email") String email,
                                    @Field("pswd") String pswd);

    @POST("newMark")
    @FormUrlEncoded
    Observable<String> addMarker(@Field("idUser") String idUser,
                                 @Field("nome") String name,
                                 @Field("desc") String desc,
                                 @Field("img") String img,
                                 @Field("lat") float lat,
                                 @Field("long") float lon);
}
