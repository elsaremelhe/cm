package com.example.cmapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.cmapp.Classes.ClusterMarker;

import com.example.cmapp.LoginActivity;
import com.example.cmapp.MapActivity;
import com.example.cmapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MarkersAdapter extends RecyclerView.Adapter<MarkersAdapter.MarkerViewHolder>{

    Context context;
    List<ClusterMarker> markerList;
    public ClusterMarker marker = null;


    public MarkersAdapter(Context context, List<ClusterMarker> markerList) {
        this.context = context;
        this.markerList = markerList;
    }

    @Override
    public MarkerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.marker_row,parent,false);
        MarkerViewHolder mvh = new MarkerViewHolder(v);
        return mvh;
    }

    @Override
    public void onBindViewHolder(MarkerViewHolder holder, int position) {

        holder.tvSnippet.setText(markerList.get(position).getSnippet());
        holder.tvImg.setImageBitmap(markerList.get(position).getIconPicture());
        holder.adapter = this;
        holder.markerVH = markerList.get(position);
    }

    @Override
    public int getItemCount() {
        return markerList.size();
    }


    public class MarkerViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{

        TextView tvSnippet;
        ImageView tvImg;
        MarkersAdapter adapter = null;
        ClusterMarker markerVH = null;

        public MarkerViewHolder(View itemView) {
            super(itemView);

            tvSnippet = itemView.findViewById(R.id.mrSnippet);
            tvImg = itemView.findViewById(R.id.mrImage);

            itemView.setOnCreateContextMenuListener(this);

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

            menu.add(0, 0, 0, R.string.noteEdit);
            menu.add(0, 1, 0, R.string.noteDelete);

            this.adapter.marker = this.markerVH;

        }


    }


    public void removeItem(){
        RequestQueue queue = Volley.newRequestQueue(context.getApplicationContext());

        StringRequest deleteRequest = new StringRequest(Request.Method.POST, "http://192.168.1.3:3000/deletemarker",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Toast.makeText(context.getApplicationContext(), "yay c:", Toast.LENGTH_SHORT).show();
                        markerList.remove(marker);
                        notifyDataSetChanged();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context.getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", marker.getId());

                return params;
            }
        };

        queue.add(deleteRequest);
    }

}

