package com.example.cmapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cmapp.Classes.Note;
import com.example.cmapp.MainActivity;
import com.example.cmapp.NewNoteActivity;
import com.example.cmapp.R;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.NotesViewHolder>{

    Context context;
    List<Note> noteList = new ArrayList<>();
    public Note note = null;


    public NotesAdapter(Context context, List<Note> noteList) {
        this.context = context;
        this.noteList = noteList;
    }

    @Override
    public NotesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.note_row,parent,false);
        NotesViewHolder nvh = new NotesViewHolder(v);
        return nvh;
    }

    @Override
    public void onBindViewHolder(NotesViewHolder holder, int position) {
        holder.tvTitle.setText(noteList.get(position).getTitle());
        holder.tvNote.setText(noteList.get(position).getNote());
        holder.adapter = this;
        holder.noteVH = noteList.get(position);
    }

    @Override
    public int getItemCount() {
        return noteList.size();
    }


    public class NotesViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        TextView tvTitle,tvNote;
        NotesAdapter adapter = null;
        Note noteVH = null;
        public NotesViewHolder(View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tvNoteTitle);
            tvNote = itemView.findViewById(R.id.tvNoteText);

            itemView.setOnCreateContextMenuListener(this);

        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

            menu.add(0, 0, 0, R.string.noteEdit);
            menu.add(0, 1, 0, R.string.noteDelete);

            this.adapter.note = this.noteVH;

        }

    }

    public void removeItem(){

        noteList.remove(note);
        notifyDataSetChanged();
    }

    public void updateItem(){
        notifyDataSetChanged();
    }

}
