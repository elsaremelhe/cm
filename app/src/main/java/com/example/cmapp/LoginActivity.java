package com.example.cmapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.cmapp.Retrofit.IMyService;
import com.example.cmapp.Retrofit.RetrofitClient;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class LoginActivity extends AppCompatActivity {

    MaterialEditText loginUser, loginPass;
    Button loginBtn;

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //initiate view
        loginUser = (MaterialEditText)findViewById(R.id.loginUser);
        loginPass = (MaterialEditText)findViewById(R.id.loginPass);
        loginBtn = (Button)findViewById(R.id.loginBtn);

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn(loginUser.getText().toString(), loginPass.getText().toString());
            }
        });

    }

    public void signAsGuest(View v){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);

    }

    public void signUp(View v){
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);

    }

    private void signIn(final String sUser, final String sPass){
        if(TextUtils.isEmpty(sUser) || TextUtils.isEmpty(sPass)){
            Toast.makeText(this, R.string.markerEditorError, Toast.LENGTH_SHORT).show();
            return;
        }

        RequestQueue queue = Volley.newRequestQueue(this);

        StringRequest loginRequest = new StringRequest(Request.Method.POST, "http://192.168.1.3:3000/login",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Toast.makeText(LoginActivity.this, R.string.welcome, Toast.LENGTH_SHORT).show();
                        try {
                            JSONObject res = new JSONObject(response);
                            Intent intent = new Intent(LoginActivity.this, MapActivity.class);
                            Bundle bun = new Bundle();
                            bun.putString("id", res.getJSONObject("message").getString("idUser"));
                            bun.putString("name", res.getJSONObject("message").getString("name"));
                            intent.putExtras(bun);
                            startActivity(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, R.string.login_failed, Toast.LENGTH_SHORT).show();
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", sUser);
                params.put("pswd", sPass);

                return params;
            }
        };

        queue.add(loginRequest);

    }
}












