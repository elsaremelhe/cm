package com.example.cmapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cmapp.Adapters.NotesAdapter;
import com.example.cmapp.DB.DBHelper;
import com.example.cmapp.Util.Ajuda;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditNoteActivity extends AppCompatActivity {

    @BindView(R.id.etTitle)
    EditText etTitle;

    @BindView(R.id.etNote)
    EditText etNote;

    NotesAdapter adapter = Ajuda.getInstance().notesAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        ButterKnife.bind(this);


        etTitle.setText(adapter.note.getTitle());
        etNote.setText(adapter.note.getNote());
    }


    private void editNote(){

        String title = etTitle.getText().toString();
        String note_text = etNote.getText().toString();


        if(title.equals("") || note_text.equals("")){
            showToast("Please fill all the fields before saving");
        }else{
            DBHelper db = new DBHelper(this);

            db.editNote(title, note_text, adapter.note);
            db.close();


            Intent i = new Intent(EditNoteActivity.this,MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);

        }

    }

    private void showToast(String msg){
        Toast.makeText(this,msg,Toast.LENGTH_SHORT);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_note,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id == R.id.action_save){
            editNote();
            adapter.updateItem();
            Toast.makeText(this, R.string.notemarkeredited, Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }
}